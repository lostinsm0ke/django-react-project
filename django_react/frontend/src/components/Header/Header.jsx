import React from "react";
import './Header.css'


const name = <a>Django+React App</a>
const logoLink = <a className="logo">{name}</a>
const descriptions = <a>Оглавление</a>
const struct = <a>Структура</a>
const opinion = <a>Заключение</a>

function Header() {
    return(
        <div className="header">
            {logoLink}
            <div className="header-right">
                {descriptions}
                {struct}
                {opinion}
            </div>
        </div>
    );
}

export default Header;

