import React from "react";
import { useState } from "react";
import "./app.css";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import { ReactDOM } from "react";
import { render } from "react-dom";

function App() {
  const [modalActive, setModalActive] = useState(true)
  return (
    <div className="App">
      <Header />
      <button className="btn" onClick={() => setModalActive(true)}>Open form</button>
      <Modal active={modalActive} setActive={setModalActive}>
        <form action="">
          <p>Имя:</p>
          <input className="input_name" type="text" /><br />
          <p>Описание:</p>
          <input className="input_description" type="text" /><br />
          <p>Загрузить фото:</p>
          <input className="input_file" type="file" /><br />
          <button className="submit_button" type="submit">Отправить</button>
        </form>
      </Modal>
    </div>
  );
}

export default App;

const container = document.getElementById("app");
render(<App />, container);
