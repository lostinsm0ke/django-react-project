from django.db import models

# Create your models here.
class Lead(models.Model):
    input_name = models.CharField(max_length=100)
    input_description = models.CharField(max_length=100)
    input_file = models.FileField(max_length=300)
